﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EdForum.DataAccessLayer.Repository.IRepositoryClasses
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAllCategoryList();
        T GetCategory(Expression<Func<T, bool>> filter);
        void AddCategory(T entity);
        void RemoveCategory(T entity);
        void RemoveInRange(IEnumerable<T> entity);
    }
}
