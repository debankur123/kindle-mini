﻿using EdForum.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdForum.DataAccessLayer.Repository.IRepositoryClasses
{
    public interface ICategoryRepo : IRepository<Category>
    {
        void UpdateCategory(Category ct);
    }
}
