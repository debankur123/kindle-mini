﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdForum.DataAccessLayer.Repository.IRepositoryClasses
{
    public interface IUnitOfWork
    {
        ICategoryRepo Category { get; }
        IProductRepository Product { get; }
        void Save();
    }
}
