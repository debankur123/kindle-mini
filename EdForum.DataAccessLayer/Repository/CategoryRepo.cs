﻿using EdForum.DataAccessLayer.Data;
using EdForum.DataAccessLayer.Repository.IRepositoryClasses;
using EdForum.Models.Models;


namespace EdForum.DataAccessLayer.Repository
{
    public class CategoryRepo : Repository<Category>, ICategoryRepo
    {
        private readonly AppDbContext _appDbContext;
        public CategoryRepo(AppDbContext context) : base(context)
        {
            _appDbContext = context;
        }
        public void UpdateCategory(Category ct)
        {
            _appDbContext.Categories.Update(ct);
        }
    }
}
