﻿using EdForum.DataAccessLayer.Data;
using EdForum.DataAccessLayer.Repository.IRepositoryClasses;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EdForum.DataAccessLayer.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly AppDbContext? _context;
        internal DbSet<T> dataSet;
        public Repository(AppDbContext context)
        {
            _context = context;
            this.dataSet = _context.Set<T>();
        }
        public void AddCategory(T entity)
        {
            dataSet.Add(entity);
        }

        public IEnumerable<T> GetAllCategoryList()
        {
            IQueryable<T> query = dataSet;
            return query.ToList();
        }

        public T GetCategory(Expression<Func<T, bool>> filter)
        {
            IQueryable<T> query = dataSet;
            query = query.Where(filter);
            return query.FirstOrDefault();
        }

        public void RemoveCategory(T entity)
        {
            dataSet.Remove(entity);
        }

        public void RemoveInRange(IEnumerable<T> entity)
        {
            dataSet.RemoveRange(entity);
        }
    }
}
