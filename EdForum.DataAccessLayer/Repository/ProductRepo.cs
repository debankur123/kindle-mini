﻿using EdForum.DataAccessLayer.Data;
using EdForum.DataAccessLayer.Repository.IRepositoryClasses;
using EdForum.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdForum.DataAccessLayer.Repository
{
    public  class ProductRepo : Repository<Product>, IProductRepository
    {
        private AppDbContext _appDbContext;
        public ProductRepo(AppDbContext context) : base(context)
        {
            _appDbContext = context;
        }
        public void UpdateProduct(Product prod)
        {
            _appDbContext.Products.Update(prod);
        }
    }
}
