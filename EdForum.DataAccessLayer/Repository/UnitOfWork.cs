﻿using EdForum.DataAccessLayer.Data;
using EdForum.DataAccessLayer.Repository.IRepositoryClasses;

namespace EdForum.DataAccessLayer.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _appDbContext;
        public ICategoryRepo Category { get; private set; }
        public IProductRepository Product { get; private set; }
        public UnitOfWork(AppDbContext context)
        {
            _appDbContext = context;
            Category = new CategoryRepo(context);
            Product = new ProductRepo(context);
        }
        public void Save()
        {
            _appDbContext.SaveChanges();
        }
    }
}
