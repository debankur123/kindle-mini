using EdForum.DataAccessLayer.Repository.IRepositoryClasses;
using EdForum.Models.Models;
using Microsoft.AspNetCore.Mvc;

namespace EdForum.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class CategoryController : Controller
    {
        private readonly IUnitOfWork _unitofwork;
        public CategoryController(IUnitOfWork unitofwork)
        {
            _unitofwork = unitofwork;
        }

        #region Adding Category
        public IActionResult Index()
        {
            List<Category> objCategoryList = _unitofwork.Category.GetAllCategoryList().ToList();
            return View(objCategoryList);
        }
        [HttpGet]
        public IActionResult AddCategory()
        {
            return View();
        }
        [HttpPost]
        public IActionResult AddCategory(Category _ct)
        {
            // Adding custom validations
            if (_ct.Name == _ct.DisplayedOrder.ToString())
            {
                ModelState.AddModelError("", "Name and Displayed Order cannot be same");
            }
            if (ModelState.IsValid)
            {
                _unitofwork.Category.AddCategory(_ct);
                _unitofwork.Save();
                TempData["success"] = "Category created Successfully";
                return RedirectToAction("Index");
            }
            return View();
        }
        #endregion Add

        #region Edit
        [HttpGet]
        public IActionResult EditCategory(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }
            Category? category = _unitofwork.Category.GetCategory(x => x.Id == id);
            if (category == null)
            {
                return NotFound();
            }
            return View(category);
        }
        [HttpPost]
        public IActionResult EditCategory(Category _category)
        {
            if (ModelState.IsValid)
            {
                _unitofwork.Category.UpdateCategory(_category);
                _unitofwork.Save();
                TempData["success"] = "Category updated successfully";
                return RedirectToAction("Index");
            }
            return View();
        }
        #endregion edit

        #region Deleting records or Removing Categories
        [HttpGet]
        public IActionResult DeleteCategory(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }
            Category? category = _unitofwork.Category.GetCategory(x => x.Id == id);
            if (category == null)
            {
                return NotFound();
            }
            return View(category);
        }
        [HttpPost, ActionName("DeleteCategory")]
        public IActionResult DeleteCategoryPOST(int? id)
        {
            Category? category = _unitofwork.Category.GetCategory(y => y.Id == id);
            if (category == null)
            {
                return NotFound();
            }
            _unitofwork.Category.RemoveCategory(category);
            _unitofwork.Save();
            TempData["success"] = "Category deleted successfully";
            return RedirectToAction("Index");
        }

        #endregion
    }
}