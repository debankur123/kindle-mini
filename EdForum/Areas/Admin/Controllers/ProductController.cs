using EdForum.DataAccessLayer.Repository.IRepositoryClasses;
using EdForum.Models.Models;
using Microsoft.AspNetCore.Mvc;

namespace EdForum.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ProductController : Controller
    {
        private readonly IUnitOfWork _unitofwork;
        public ProductController(IUnitOfWork unitofwork)
        {
            _unitofwork = unitofwork;
        }

        #region Adding Product
        public IActionResult Index()
        {
            List<Product> objProductList = _unitofwork.Product.GetAllCategoryList().ToList();
            return View(objProductList);
        }
        [HttpGet]
        public IActionResult AddProduct()
        {
            return View();
        }
        [HttpPost]
        public IActionResult AddProduct(Product _ct)
        {
            if (ModelState.IsValid)
            {
                _unitofwork.Product.AddCategory(_ct);
                _unitofwork.Save();
                TempData["success"] = "Product created Successfully";
                return RedirectToAction("Index");
            }
            return View();
        }
        #endregion Add

        #region Edit
        [HttpGet]
        public IActionResult EditProduct(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }
            Product? product = _unitofwork.Product.GetCategory(x => x.Id == id);
            if (product == null)
            {
                return NotFound();
            }
            return View(product);
        }
        [HttpPost]
        public IActionResult EditProduct(Product _product)
        {
            if (ModelState.IsValid)
            {
                _unitofwork.Product.UpdateProduct(_product);
                _unitofwork.Save();
                TempData["success"] = "Product updated successfully";
                return RedirectToAction("Index");
            }
            return View();
        }
        #endregion edit

        #region Deleting records or Removing Products
        [HttpGet]
        public IActionResult DeleteProduct(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }
            Product? product = _unitofwork.Product.GetCategory(x => x.Id == id);
            if (product == null)
            {
                return NotFound();
            }
            return View(product);
        }
        [HttpPost, ActionName("DeleteProduct")]
        public IActionResult DeleteProductPOST(int? id)
        {
            Product? product = _unitofwork.Product.GetCategory(y => y.Id == id);
            if (product == null)
            {
                return NotFound();
            }
            _unitofwork.Product.RemoveCategory(product);
            _unitofwork.Save();
            TempData["success"] = "Product deleted successfully";
            return RedirectToAction("Index");
        }

        #endregion
    }
}