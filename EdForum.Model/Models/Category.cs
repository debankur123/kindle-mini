﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EdForum.Models.Models;

public class Category
{
    [Key]
    public int Id { get; set; }
    [Required]
    [DisplayName("Category Name")]
    [MaxLength(70,ErrorMessage ="Name cannot exceed 70 characters")]
    public string? Name { get; set; }
    [DisplayName("Displayed Order")] //Used Data Annotations here.
    [Range(1,50,ErrorMessage ="Order should range between 1-50")]
    public int DisplayedOrder { get; set; }
    [DisplayName("Created On")]
    public DateTime CreatedOn { get; set; } = DateTime.Now;
}